import React, { useState } from 'react';
import './App.css';

const Calculator = _ => {
    const [result, setPlus] = useState(0);
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    return (<div className="App">
      <header className="App-header">
        <input name="x" type="text" value={x} onChange={e => setX(e.target.x)} />
        <input name="y" type="text" value={y} onChange={e => setY(e.target.y)} />
        <button onClick={_ => {setPlus(parseInt(x) + parseInt(y))}}>Plus</button>
        <p>Result: {result}</p>
      </header>
    </div>)
}

export default Calculator;