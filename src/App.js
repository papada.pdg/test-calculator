import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      x: 0,
      y: 0,
      result: 0
   }
  }
  plus = _ => {
   this.setState({result: parseInt(this.state.x) + parseInt(this.state.y)})
 }
 
  fieldChange = e => {
   this.setState({ [e.target.name]: e.target.value })
 }

  render() {
    return (<div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <input name="x" type="text" value={this.state.x} onChange={this.fieldChange} />
        <input name="y" type="text" value={this.state.y} onChange={this.fieldChange} />
        <button onClick={this.plus}>Plus</button>
        <p>Result: {this.state.result}</p>
      </header>
    </div>)
  }
}

export default App;
